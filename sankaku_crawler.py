import sys, os, urllib, urllib2, time

flag = ""; path = ""
limit = 1000
tags = []; list = []

from bs4 import BeautifulSoup

for arg in sys.argv:
    # when arg is its file name
    if os.path.basename(__file__) == arg: continue
    
    if arg[0] == '-':
        if arg[1] == '-':
            if arg[2:] == 'limit':
                flag = 'n'
            if arg[2:] == 'tag':
                flag = 't'
        else :
            flag = arg[1]
    else :
        if flag != "":
            if flag == 'n':
                limit = int(arg)
            if flag == 't':
                tags.append(arg)
            flag = ""
        else:
            path = arg + '/'
            if not os.path.isdir(path):
                os.makedirs(path)

# tag = cosplay & dress
# tag (nopage) = lace & saya & gloves

url_original = "http://idol.sankakucomplex.com/post"
url = "http://idol.sankakucomplex.com/post/index?tags=" + "+".join(tags) + "&page=1"

while True:
    time.sleep(100.0 / 1000.0)

    req = urllib2.Request(url)
    req.add_unredirected_header('User-Agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.71 Safari/537.36')
    response = urllib2.urlopen(req)
    html = response.read()

    soup = BeautifulSoup(html)
    flag = 0
    nexturl = ""

    for div1 in soup('div'):
        try:
            if div1['id'] != 'content': continue

            soup2 = BeautifulSoup(str(div1))
            for div2 in soup2('div'):

                try:
                    if div2['class'][0] != 'content': continue

                    soup3 = BeautifulSoup(str(div2))

                    for div3 in soup3('div'):
                        try:
                            if div3['next-page-url'] != "":
                                nexturl = str(div3['next-page-url'])
                        except:
                            temp = 111


                    for div3 in soup3('span'):
                        try:
                            if div3['class'][0] == "popular-rank":
                                flag = flag + 1

                            if div3['class'][0] == "thumb":
                                if flag > 0: 
                                    flag -= 1
                                    continue

                                soup4 = BeautifulSoup(str(div3))
                                for div4 in soup4('a'):
                                    if limit == 0: break

                                    time.sleep(200.0 / 1000.0)

                                    redirect_url = "http://idol.sankakucomplex.com" + div4['href']
                                    req2 = urllib2.Request(redirect_url)
                                    req2.add_unredirected_header('User-Agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.71 Safari/537.36')
                                    response2 = urllib2.urlopen(req2)
                                    html2 = response2.read()

                                    start_idx = html2.index("Original:")
                                    image_url = ""
                                    start_flag = False

                                    for i in range(start_idx,len(html2)):
                                        if html2[i] == '\"':
                                            if start_flag: break
                                            else: start_flag = True; continue

                                        if start_flag == True: image_url += html2[i]

                                    list.append(image_url)

                                    print '.',

                                    limit = limit - 1
                        except:
                            temp = 111
                        if limit == 0: break

                except:
                    temp = 111
                if limit == 0: break

        except:
            temp = 111
        if limit == 0: break

    if limit == 0 or nexturl == "": break
    url = url_original + nexturl

print '\n'

for imgurl in list:
    sp = imgurl.split('/')
    file_name = sp[len(sp)-1]

    print "downloading: " + file_name + " from " + imgurl
    urllib.urlretrieve(imgurl, path + file_name)

    time.sleep(1)
